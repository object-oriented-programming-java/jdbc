import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by leanderson on 18/11/16.
 */
public class CarroDAO {

    private Connection mConnection;

    public CarroDAO(){
        mConnection = (new ConnectionFactory()).getConnection();
    }

    public void inserir(Carro carro){

        String sql = "insert into carro" +
                " (nome, qtd_portas, ano_fabricacao, ano_modelo, marca, cor)" +
                " values (?,?,?,?,?,?)"; // no lugar dos valores deve colocar ?

        try{

            // faz um monte de operações.
            // que podem lançar exceptions runtime e SQLException

            PreparedStatement stmt = mConnection.prepareStatement(sql);

            // preenche os valores
            stmt.setString(1, carro.getNome());  // deve informar o indice do ? que deseja substituir pelo valor
            stmt.setInt(2, carro.getQtdPortas());
            stmt.setDate(3, new java.sql.Date(
                    carro.getAnoFabricacao().getTime()));
            stmt.setDate(4, new java.sql.Date(
                    carro.getAnoModelo().getTime()));
            stmt.setString(5, carro.getMarca());
            stmt.setString(6, carro.getCor());

            // executa
            stmt.execute();
            stmt.close();

            System.out.println("Gravado!");

        }catch(SQLException e){
            System.out.println(e);
        }
    }

    public void atualizar(Carro carro){

        String sql = "update carro" +
                "set        nome = ?," +
                "     qtd_portas = ?," +
                " ano_fabricacao = ?," +
                "     ano_modelo = ?," +
                "          marca = ?" +
                "           ,cor = ?" +
                " where id_carro = ?"; // no lugar dos valores deve colocar ?

        try{

            // faz um monte de operações.
            // que podem lançar exceptions runtime e SQLException

            PreparedStatement stmt = mConnection.prepareStatement(sql);

            // preenche os valores
            stmt.setString(1, carro.getNome());  // deve informar o indice do ? que deseja substituir pelo valor
            stmt.setInt(2, carro.getQtdPortas());
            stmt.setDate(3, new java.sql.Date(
                    carro.getAnoFabricacao().getTime()));
            stmt.setDate(4, new java.sql.Date(
                    carro.getAnoModelo().getTime()));
            stmt.setString(5, carro.getMarca());
            stmt.setString(6, carro.getCor());
            stmt.setLong(7, carro.getId());

            // executa
            stmt.execute();
            stmt.close();

            System.out.println("Atualizado!");

        }catch(SQLException e){
            System.out.println(e);
        }
    }

    public void deletar(Carro carro){

        String sql = " delete from carro" +
                     " where id_carro = ?"; // no lugar dos valores deve colocar ?

        try{

            // faz um monte de operações.
            // que podem lançar exceptions runtime e SQLException

            PreparedStatement stmt = mConnection.prepareStatement(sql);

            // preenche os valores
            stmt.setLong(1, carro.getId());

            // executa
            stmt.execute();
            stmt.close();

            System.out.println("Deletado!");

        }catch(SQLException e){
            System.out.println(e);
        }
    }

    public List<Carro> obterTodos(){
        List<Carro> lista = new ArrayList<>();

        String sql = " select * from carro order by nome ";

        try {
            // faz um monte de operações.
            // que podem lançar exceptions runtime e SQLException

            PreparedStatement stmt = mConnection.prepareStatement(sql);

            ResultSet resultSet = stmt.executeQuery();

            while(resultSet.next()){

                Carro carro = new Carro();

                carro.setId(resultSet.getInt("id_carro"));
                carro.setNome(resultSet.getString("nome"));
                carro.setMarca(resultSet.getString("marca"));
                carro.setCor(resultSet.getString("cor"));
                carro.setQtdPortas(resultSet.getInt("qtd_portas"));
                carro.setAnoFabricacao(new  java.util.Date(resultSet.getDate("ano_fabricacao").getTime()));
                carro.setAnoModelo(new  java.util.Date(resultSet.getDate("ano_modelo").getTime()));

                lista.add(carro);
            }

        }catch(SQLException e){
            System.out.println(e);
        }


        return lista;
    }


}
