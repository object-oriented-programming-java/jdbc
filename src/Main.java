import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Date;
import java.util.List;

/**
 * Created by leanderson on 09/11/16.
 */
public class Main {

    public static void main(String args[]){

        // Inserir um novo carro
        Carro carro = new Carro();
        carro.setNome("Renegade");
        carro.setMarca("Jeep");
        carro.setCor("Preto");
        carro.setQtdPortas(4);
        carro.setAnoModelo(new Date());
        carro.setAnoFabricacao(new Date());

        CarroDAO dao = new CarroDAO();

        // Insere um novo carro
        dao.inserir(carro);

        //--
        // Atualiza o carro
        //--
        carro.setId(2);
        carro.setNome("HB20");
        carro.setMarca("Hyundai");
        carro.setCor("Cinza");

        dao.atualizar(carro);


        //--
        // Deletar o carro
        //--
        carro.setId(2);

        dao.deletar(carro);

        //--
        // Consultar carros
        //--
        List<Carro> carros = dao.obterTodos();
        for(Carro c : carros){
            System.out.println("Carro : "+c.getNome());
        }


    }

}
