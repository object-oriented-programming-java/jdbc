import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by leanderson on 10/11/16.
 */
public class ConnectionFactory {

    public Connection getConnection() {
        try {
            return DriverManager.getConnection(
                    "jdbc:mysql://localhost/meu_banco", // link de conexão
                    "root", // usuario
                    "root");// senha
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}
